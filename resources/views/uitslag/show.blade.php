@extends('layout.mvh')

@section('pagetitle', 'WedstrijdUitslag')
@section('pagedescription', 'Bekijk de wedstrijduitslag van visclub moed &amp; volharding herenthout')
@section('content')
    <br><br>
    <h3>Uitslag : {{ $wedstrijd->getDatum() }} - {{ $wedstrijd->omschrijving }}</h3><br><br>

    @if($totaal === 0)
        <br><br>
        <h3>Geen uitslag beschikbaar !</h3><br><br>
    @else
        @include('uitslag.detail')
    @endif
@endsection
