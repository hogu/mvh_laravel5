<table class="table table-striped table-hover ">
    <thead>
    <tr class="info">
        <th>Volgorde</th>
        <th>Deelnemer(s)</th>
        <th colspan="{{ $aantal_plaatsen }}">Plaats(en)</th>
        <th colspan="{{ $aantal_gewichten }}">Gewicht(en)</th>
        <th>Totaal</th>
    </tr>
    </thead>
    <tbody>
    @foreach($uitslagDetail as $detail)
        <tr>
            <td align="center">{{ $detail['volgnummer'] }}</td>
            <td>{{ $detail['deelnemers'] }}</td>
            @foreach($detail['plaatsen'] as $plaats)
                <td align="center">{{ $plaats }}</td>
            @endforeach
            @foreach($detail['gewichten'] as $gewicht)
                <td align="center">{{ number_format($gewicht, 0, ",", ".") }}</td>
            @endforeach
            <td align="center">{{ number_format($detail['totaal'], 0, ",", ".") }}</td>
        </tr>
    @endforeach
    <tr>
        <td></td>
        <td></td>
        @foreach($detail['plaatsen'] as $plaats)
            <td></td>
        @endforeach
        @foreach($detail['gewichten'] as $gewicht)
            <td></td>
        @endforeach
        <td align="center"><strong>{{ number_format($totaal, 0, ",", ".") }}</strong></td>
    </tr>
    </tbody>
</table>
