@extends('layout.mvh')

@section('pagetitle', "Kalender Archief")
@section('pagedescription', 'Bekijk het archief van een kalender met wedstrijden van visclub moed &amp; volharding herenthout')
@section('content')
    @if(isset($kalender))
        <br><br>
        <h3>Kalender Archief : {{ $kalender->omschrijving() }}</h3><br><br>

        @if($wedstrijden->isNotEmpty())
            @include('wedstrijd.overzicht')
        @else
            <h4>Geen wedstrijden beschikbaar !</h4>
        @endif
    @else
        <br><br>
        <h3>Geen kalender beschikbaar !</h3><br><br>
    @endif
@endsection