@extends('layout.mvh')

@section('pagetitle', 'Uitslag Archief')
@section('pagedescription', 'Bekijk het archief van een wedstrijduitslag van visclub moed &amp; volharding herenthout')
@section('content')
    <br><br>
    <h3>Uitslag Archief : {{ $wedstrijd->getDatum() }} - {{ $wedstrijd->omschrijving }}</h3><br><br>

    @if($totaal === 0)
        <h3>Geen uitslag beschikbaar !</h3><br><br>
    @else
        @include('uitslag.detail')
    @endif
@endsection
