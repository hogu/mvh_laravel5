<table class="table table-striped table-hover ">
    <thead>
        <tr class="info">
            <th>Kalenders</th>
        </tr>
    </thead>
    <tbody>
        @foreach($kalenders as $kalender)
            <tr>
                <td>
                    <a href="{{ URL::to('/archief/kalender/' . $kalender->id) }}">{{ $kalender->omschrijving() }}</a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
