@extends('layout.mvh')

@section('pagetitle', 'Archief')
@section('pagedescription', 'Bekijk oudere kalenders/uitslagen van visclub moed &amp; volharding herenthout')
@section('content')
    <br><br>
    <h3>Archief :</h3><br><br>

    @if(count($kalenders) > 0)

        @include('archief.kalenders.overzicht')
    @else
        <h4>Geen kalenders beschikbaar !</h4>
    @endif
@endsection