@extends('layout.mvh')

@section('pagetitle', 'Home')
@section('pagedescription', 'Bekijk de home pagina van visclub moed &amp; volharding herenthout')
@section('content')
    @if($wedstrijden->isNotEmpty())
        <br><br>
        <h3>Deze maand :</h3><br><br>

        @include('wedstrijd.overzicht')
    @else
        <br><br>
        <h3>Geen wedstrijden/evenementen deze maand !</h3><br><br>
    @endif
@endsection