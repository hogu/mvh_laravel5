@extends('layout.mvh')

@section('pagetitle', 'Uitslagen')
@section('pagedescription', 'Bekijk de recentste uitslagen van visclub moed &amp; volharding herenthout')
@section('content')
    @if(isset($kalender))
        <br><br>
        <h3>Uitslagen {{ $kalender->omschrijving() }}</h3><br><br>

        @if($wedstrijden->isNotEmpty())
            @include('wedstrijd.overzicht')
        @else
            <h4>Geen wedstrijden beschikbaar !</h4>
        @endif
    @else
        <br><br>
        <h3>Geen uitslagen beschikbaar !</h3><br><br>
    @endif
@endsection