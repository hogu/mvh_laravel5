<form class="form-horizontal">
    <div class="form-group">
        <label for="naam" class="col-sm-2 control-label">Naam</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="naam" placeholder="Naam" disabled value="{{ $club->naam }}">
        </div>
    </div>
    <div class="form-group">
        <label for="adres" class="col-sm-2 control-label">Adres</label>
        <div class="col-sm-10">
            <textarea class="form-control" id="adres" placeholder="Adres" disabled>{{ $club->adres   }}</textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="telefoon" class="col-sm-2 control-label">Telefoon</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="telefoon" placeholder="Telefoon" disabled value="{{ $club->telefoon }}">
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="email" placeholder="Email" disabled value="{{ $club->email }}">
        </div>
    </div>
    <div class="form-group">
        <label for="contactpersonen" class="col-sm-2 control-label">Contactpersonen</label>
        <div class="col-sm-10">
            @if($contactpersonen->isEmpty())
                <input type="text" class="form-control" id="contactpersonen" placeholder="Geen contactpersonen beschikbaar!" disabled value="Geen contactpersonen beschikbaar !">
            @else
                @include('contactpersoon.overzicht')
            @endif
        </div>
    </div>
</form>
