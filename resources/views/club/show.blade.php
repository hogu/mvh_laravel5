@extends('layout.mvh')

@section('pagetitle', 'Contact')
@section('pagedescription', 'Bekijk de contact gegevens van visclub moed &amp; volharding herenthout')
@section('content')
    <br><br>
    @if(isset($club))
        <h3>Contact :</h3><br><br>

        @include('club.detail')
    @else
        <h3>Geen contactgegevens beschikbaar !</h3>
    @endif
@endsection