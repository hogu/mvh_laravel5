<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a href="#" class="navbar-brand">Visclub Moed &amp; Volharding Herenthout</a>
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">

            <ul class="nav navbar-nav navbar-right">
                @include('layout.link.HomeLink')
                @include('layout.link.KalenderLink')
                @include('layout.link.UitslagenLink')
                @include('layout.link.ArchiefLink')
                @include('layout.link.ContactLink')
            </ul>

        </div>
    </div>
</div>
