<!DOCTYPE html>
<html lang="nl">
    <head>
        <title>Visclub Moed &amp; Volharding Herenthout - @yield('pagetitle')</title>
        <meta name="description" content="@yield('pagedescription')"/>
        <meta name="keywords" content="visclub,mvh,Moed,Volharding,pauwelstraat,herenthout,moed &amp; volharding,moed en volharding,visclub moed &amp; volharding,vislcub moed en volharding"/>
        <meta name="author" content="Guido Van Hoof"/>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css') }}" media="screen">
        <link rel="stylesheet" href="{{ URL::asset('css/usebootstrap.css') }}">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="{{ URL::asset('js/html5shiv.js') }}"></script>
        <script src="{{ URL::asset('js/respond.min.js') }}"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">
            <div id="navbar" class="row">
                @include('layout.navbar')
            </div>

            <div id="content" class="row">
                @yield('content')
            </div>

            <div id="footer" class="row">
                @include('layout.footer')
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/usebootstrap.js"></script>
    </body>
</html>