<ul class="nav navbar-nav">
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes">Archief<span class="caret"></span></a>
        <ul class="dropdown-menu" aria-labelledby="themes">
            @include('layout.link.ArchiefKalendersLink')
        </ul>
    </li>
</ul>
