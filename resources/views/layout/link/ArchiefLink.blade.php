@if($page === 'Archief')
    <li class="active"><a href="{{ URL::to('archief/kalenders') }}">Archief</a></li>
@else
    <li><a href="{{ URL::to('archief/kalenders') }}">Archief</a></li>
@endif
