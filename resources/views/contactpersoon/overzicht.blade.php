<table class="table table-striped table-hover ">
    <thead>
    <tr class="info">
        <th>Naam</th>
        <th>Telefoon</th>
        <th>Email</th>
    </tr>
    </thead>
    <tbody>
    @foreach($contactpersonen as $contactpersoon)
        <tr>
            <td>{{ $contactpersoon->volledigeNaam() }}</td>
            <td>{{ $contactpersoon->telefoon }}</td>
            <td>{{ $contactpersoon->email }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
