<table class="table table-striped table-hover ">
    <thead>
        <tr class="info">
            <th>Datum</th>
            <th>Omschrijving</th>
            @if($extra_detail)
                <th>Aanvang</th>
                <th>Opmerkingen</th>
            @endif
        </tr>
    </thead>
    <tbody>
        @foreach($wedstrijden as $wedstrijd)
            <tr>
                @if($show_link and $wedstrijd->totaalGewicht() > 0)
                    <td><a href="{{ URL::to($url . $wedstrijd->id) }}">{{ $wedstrijd->getDatum() }}</a></td>
                @else
                    <td>{{ $wedstrijd->getDatum() }}</td>
                @endif
                <td>{{ $wedstrijd->omschrijving }}</td>
                @if($extra_detail)
                    <td>{{ $wedstrijd->getAanvang()}}</td>
                    <td>{{ $wedstrijd->opmerkingen}}</td>
                @endif
            </tr>
        @endforeach
    </tbody>
</table>
