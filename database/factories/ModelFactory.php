<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Mvh\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(
    Mvh\Wedstrijd::class,
    function (Faker\Generator $faker) {
        return [
            'kalender_id' => 1,
            'datum' => $faker->date('Y-m-d'),
            'omschrijving' => $faker->sentence,
            'aanvang' => $faker->time(),
            'opmerkingen' => $faker->paragraph,
            'wedstrijdtype_id' => 1
        ];
    }
);

$factory->define(
    Mvh\Reeks::class,
    function (Faker\Generator $faker) {
        return [
            'volgnummer' => 1,
            'aanvang' =>$faker->time(),
            'duur' => $faker->time(),
            'opmerkingen' => $faker->paragraph,
            'zakgewicht' => 0,
        ];
    }
);

$factory->define(
    Mvh\WedstrijdType::class,
    function (Faker\Generator $faker) {
        return [
            'omschrijving' => $faker->sentence,
            'aantal_wedstrijden' => 1,
            'aantal_beste_wedstrijden' => 1
        ];
    }
);

$factory->define(
    Mvh\Plaats::class,
    function (Faker\Generator $faker) {
        return [
            'reeks_id' => 1,
            'nummer' => 1
        ];
    }
);

$factory->define(
    Mvh\WedstrijdDeelnemer::class,
    function (Faker\Generator $faker) {
        return [
            'wedstrijd_id' => 1,
            'deelnemer_id' => 1
        ];
    }
);

$factory->define(
    Mvh\Deelnemer::class,
    function (Faker\Generator $faker) {
        return [
            'nummer' => 1,
            'naam' => $faker->word,
            'voornaam' => $faker->word
        ];
    }
);

$factory->define(
    Mvh\PlaatsDeelnemer::class,
    function (Faker\Generator $faker) {
        return [
            'plaats_id' => 1,
            'wedstrijd_deelnemer_id' => 1
        ];
    }
);

$factory->define(
    Mvh\PlaatsGewicht::class,
    function (Faker\Generator $faker) {
        return [
            'plaats_id' => 1,
            'gewicht' => 1
        ];
    }
);

$factory->define(
    Mvh\Kalender::class,
    function (Faker\Generator $faker) {
        return [
            'jaar' => $faker->date('Y'),
            'opmerkingen' => $faker->sentence
        ];
    }
);

$factory->define(
    Mvh\Club::class,
    function (Faker\Generator $faker) {
        return [
            'naam' => $faker->word,
            'adres' => 'STRAAT + NUMMER' . chr(10) . 'POSTCODE + GEMEENTE',
            'telefoon' => '014 / 51 33 77',
            'email' => $faker->email
        ];
    }
);

$factory->define(
    Mvh\ContactPersoon::class,
    function (Faker\Generator $faker) {
        return [
            'club_id' => 1,
            'naam' => $faker->word,
            'voornaam' => $faker->word,
            'adres' => 'STRAAT + NUMMER' . chr(10) . 'POSTCODE + GEMEENTE',
            'telefoon' => '014 / 51 33 77',
            'email' => $faker->email
        ];
    }
);