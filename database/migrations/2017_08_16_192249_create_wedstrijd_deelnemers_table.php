<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWedstrijdDeelnemersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wedstrijddeelnemers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('wedstrijd_id');
            $table->integer('deelnemer_id');
            $table->timestamps();

            $table->unique(['wedstrijd_id', 'deelnemer_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wedstrijddeelnemers');
    }
}
