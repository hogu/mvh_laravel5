<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaatsenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plaatsen', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reeks_id');
            $table->smallInteger('nummer');
            $table->timestamps();

            $table->unique(['reeks_id', 'nummer']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plaatsen');
    }
}