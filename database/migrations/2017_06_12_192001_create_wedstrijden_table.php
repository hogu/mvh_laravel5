<?php

use Mvh\Wedstrijd;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWedstrijdenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'wedstrijden', 
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('kalender_id');
                $table->date('datum')->unique();
                $table->string('omschrijving');
                $table->time('aanvang')->default(Wedstrijd::DEFAULT_AANVANG);
                $table->text('opmerkingen')->nullable();
                $table->integer('wedstrijdtype_id')->unsigned();
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wedstrijden');
    }
}
