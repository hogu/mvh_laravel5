<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactPersoonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contactpersonen', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('club_id');
            $table->string('naam', 100);
            $table->string('voornaam', 100);
            $table->text('adres');
            $table->string('telefoon', 50);
            $table->string('email', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contactpersonen');
    }
}
