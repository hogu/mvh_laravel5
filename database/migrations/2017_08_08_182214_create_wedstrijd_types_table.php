<?php

use Mvh\WedstrijdType;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWedstrijdTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wedstrijdtypes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('omschrijving');
            $table->smallinteger('aantal_wedstrijden')->default(WedstrijdType::DEFAULT_AANTAL_WEDSTRIJDEN);
            $table->smallinteger('aantal_beste_wedstrijden')->default(WedstrijdType::DEFAULT_AANTAL_BESTE_WEDSTRIJDEN);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wedstrijdtypes');
    }
}
