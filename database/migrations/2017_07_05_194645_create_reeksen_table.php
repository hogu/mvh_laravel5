<?php

use Mvh\Reeks;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReeksenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reeksen', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('wedstrijd_id')->unsigned();
            $table->unsignedSmallInteger('volgnummer')->default(Reeks::DEFAULT_VOLGNUMMER)->unique();
            $table->time('aanvang')->default(Reeks::DEFAULT_AANVANG);
            $table->time('duur')->default(Reeks::DEFAULT_DUUR);
            $table->integer('zakgewicht')->unsigned()->default(Reeks::DEFAULT_ZAKGEWICHT);
            $table->text('opmerkingen')->nullable();
            $table->timestamps();

            $table->foreign('wedstrijd_id')
                ->references('id')
                ->on('wedstrijden');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reeksen');
    }
}
