<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaatsDeelnemersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plaatsdeelnemers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('plaats_id');
            $table->integer('wedstrijd_deelnemer_id');
            $table->timestamps();

            $table->unique(['plaats_id', 'wedstrijd_deelnemer_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plaatsdeelnemers');
    }
}
