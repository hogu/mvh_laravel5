<?php

namespace Mvh;

use Illuminate\Database\Eloquent\Model;

class WedstrijdDeelnemer extends Model
{
    protected $table = 'wedstrijddeelnemers';

    public function wedstrijd()
    {
        return $this->belongsTo('Mvh\Wedstrijd');
    }

    public function deelnemer()
    {
        return $this->belongsTo('Mvh\Deelnemer');
    }
}
