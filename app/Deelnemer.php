<?php

namespace Mvh;

use Illuminate\Database\Eloquent\Model;

class Deelnemer extends Model
{
    protected $fillable = ['nummer', 'naam', 'voornaam'];

    public function setNaamAttribute($naam)
    {
        $this->attributes['naam'] = strtoupper($naam);
    }

    public function setVoornaamAttribute($voornaam)
    {
        $this->attributes['voornaam'] = ucfirst($voornaam);
    }

    public function volledigeNaam()
    {
        return $this->naam . ' ' . $this->voornaam;
    }
}
