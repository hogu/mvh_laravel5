<?php

namespace Mvh;

use Mvh\Wedsrrijd;
use Illuminate\Database\Eloquent\Model;

class Reeks extends Model
{
    const DEFAULT_AANVANG = '13:00:00';
    const DEFAULT_DUUR = '02:00:00';
    const DEFAULT_ZAKGEWICHT = 0;
    const DEFAULT_VOLGNUMMER = 1;

    protected $table = 'reeksen';
    protected $fillable = [
        'aanvang', 'duur', 'opmerkingen', 'zakgewicht', 'wedstrijd_id', 'volgnummer'
    ];

    public function getAanvang()
    {
        return substr($this->aanvang, 0, 5);
    }

    public function getDuur()
    {
        return substr($this->duur, 0, 5);
    }

    public function wedstrijd()
    {
        return $this->belongsTo('Mvh\Wedstrijd');
    }

    public function plaatsen()
    {
        return $this->hasMany('Mvh\Plaats');
    }
}
