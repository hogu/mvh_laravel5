<?php

namespace Mvh;

use Illuminate\Database\Eloquent\Model;

class Kalender extends Model
{
    const PREFIX_OMSCHRIJVING = 'Wedstrijdkalender ';

    protected $fillable = [
        'jaar', 'opmerkingen'
    ];

    public function wedstrijden()
    {
        return $this->hasMany('Mvh\Wedstrijd')->orderBy('datum');
    }

    public function omschrijving()
    {
        return self::PREFIX_OMSCHRIJVING . $this->jaar;
    }

    public static function recentste()
    {
        return Kalender::orderBy('jaar', 'desc')->first();
    }
}
