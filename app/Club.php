<?php

namespace Mvh;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    protected $fillable = ['naam', 'adres', 'telefoon', 'email'];

    public function contactPersonen()
    {
        return $this->hasMany('Mvh\ContactPersoon');
    }
}
