<?php

namespace Mvh;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Wedstrijd extends Model
{
    const DEFAULT_AANVANG = "13:30:00";

    protected $table = 'wedstrijden';
    protected $fillable = [
        'datum', 'omschrijving', 'aanvang', 'opmerkingen', 'wedstrijdtype_id', 'kalender_id'
    ];

    public function getAanvang()
    {
        return substr($this->aanvang, 0, 5);
    }

    public function reeksen()
    {
        return $this->hasMany('Mvh\Reeks');
    }
    
    public function type()
    {
        return $this->belongsTo('Mvh\WedstrijdType', 'wedstrijdtype_id');
    }

    public function deelnemers()
    {
        return $this->hasMany('Mvh\WedstrijdDeelnemer');
    }

    public function uitslagDetail()
    {
        $uitslagDetails =  DB::select(
            DB::raw(
                "select w.*
                        , pd.wedstrijd_deelnemer_id
                        , p.nummer
                        , pg.gewicht - r.zakgewicht deelgewicht
                        , concat(d.naam,  ' ', d.voornaam) deelnemer
                        , ag.aantal_gewichten
                        , ar.aantal_reeksen
                        , t.totaal
                    from wedstrijd_uitslag w
                        join (
                            select plaats_id, round(exp(sum(log(coalesce(wedstrijd_deelnemer_id, 1))))) deelnemer_ids
                            from plaatsdeelnemers
                            group by plaats_id
                        ) di on w.deelnemer_ids = di.deelnemer_ids
                        join plaatsdeelnemers pd on pd.plaats_id = di.plaats_id
                        join plaatsgewichten pg on pg.plaats_id = di.plaats_id
                        join plaatsen p on p.id = di.plaats_id
                        join reeksen r on r.id = p.reeks_id
                        join wedstrijddeelnemers wd on wd.id = pd.wedstrijd_deelnemer_id
                        join deelnemers d on d.id = wd.deelnemer_id
                        join (
                            select a.wedstrijd_id, max(a.aantal_gewichten) aantal_gewichten
                            from (
                                select w.wedstrijd_id
                                    , pd.wedstrijd_deelnemer_id
                                    , count(pg.gewicht) aantal_gewichten
                                from wedstrijd_uitslag w
                                    join deelnemer_ids di on w.deelnemer_ids = di.deelnemer_ids
                                    join plaatsdeelnemers pd on pd.plaats_id = di.plaats_id
                                    join plaatsgewichten pg on pg.plaats_id = di.plaats_id
                                    join plaatsen p on p.id = di.plaats_id
                                    join reeksen r on r.id = p.reeks_id
                                    join wedstrijddeelnemers wd on wd.id = pd.wedstrijd_deelnemer_id
                                    join deelnemers d on d.id = wd.deelnemer_id
                                group by w.wedstrijd_id, pd.wedstrijd_deelnemer_id
                            ) a
                            group by a.wedstrijd_id
                        ) ag on ag.wedstrijd_id = w.wedstrijd_id
                        join (
                            select wedstrijd_id, count(volgnummer) as aantal_reeksen
                            from reeksen
                            group by wedstrijd_id
                        ) ar on ar.wedstrijd_id = w.wedstrijd_id
                        join (
                            select wedstrijd_id, sum(gewicht)as totaal
                            from wedstrijd_uitslag
                            group by wedstrijd_id
                        ) t on t.wedstrijd_id = w.wedstrijd_id
                    where w.wedstrijd_id = ?
                    order by wedstrijd_id, w.gewicht desc, concat(d.naam, ' ', d.voornaam), r.volgnummer"
            ),
            [$this->id]
        );

        $uitslagDetail = [];
        $vorig_deelnemer_ids ='';
        $vorig_nummer = 0;
        $volgnummer = 0;
        $plaatsen = [];
        $gewichten = [];
        $totaal = 0;
        $deelnemers = [];
        $aantal_plaatsen = 0;
        $aantal_gewichten = 0;
        $aantalPlaatsen = 0;
        $aantalGewichten = 0;
        $wedstrijdTotaal = 0;
        foreach ($uitslagDetails as $detail)
        {
            $wedstrijdTotaal = $detail->totaal;
            if ($vorig_deelnemer_ids <> $detail->deelnemer_ids)
            {
                if ($vorig_deelnemer_ids <> '') {
                    $uitslagDetail[] = [
                        'volgnummer' => $volgnummer,
                        'deelnemers' => implode(' - ', $deelnemers),
                        'plaatsen' => $plaatsen,
                        'gewichten' => $gewichten,
                        'totaal' => $totaal
                    ];
                } else {
                    $aantal_plaatsen = $detail->aantal_reeksen;
                    $aantal_gewichten = $detail->aantal_gewichten;
                }

                $volgnummer++;
                $deelnemers = [];
                $plaatsen = [];
                $gewichten = [];
                $aantalPlaatsen = 0;
                $aantalGewichten = 0;
                $totaal = $detail->gewicht;
                $vorig_nummer = '';
            }
            if (! in_array($detail->deelnemer, $deelnemers))
            {
                $deelnemers[] = $detail->deelnemer;
            }
            if ($aantalPlaatsen < $aantal_plaatsen) {
                $plaatsen[] = $detail->nummer === $vorig_nummer ? '' : $detail->nummer;
                $aantalPlaatsen++;
            }
            if ($aantalGewichten < $aantal_gewichten) {
                $gewichten[] = $detail->deelgewicht;
                $aantalGewichten++;
            }
            $vorig_deelnemer_ids = $detail->deelnemer_ids;
            $vorig_nummer = $detail->nummer;
        }
        $uitslagDetail[] = [
            'volgnummer' => $volgnummer,
            'deelnemers' => implode(' - ', $deelnemers),
            'plaatsen' => $plaatsen,
            'gewichten' => $gewichten,
            'totaal' => $totaal
        ];

        return [
            'totaal' => $wedstrijdTotaal,
            'aantal_plaatsen' => $aantal_plaatsen,
            'aantal_gewichten' => $aantal_gewichten,
            'details' => $uitslagDetail
        ];
    }

    private function containsNot($needle, $haystack)
    {
        return strpos($haystack, $needle) === false;
    }

    public function kalender()
    {
        return $this->belongsTo('Mvh\Kalender');
    }

    public function getDatum()
    {
        return (new Carbon($this->datum))->format('d/m/Y');
    }

    public function totaalGewicht()
    {
        $totaal = DB::select(
            'select coalesce(sum(totaal), 0) totaal from wedstrijd_uitslag_totaal where wedstrijd_id = ?',
            [$this->id]
        );

        return $totaal[0]->totaal;
    }

    public static function inJaarMaand($jaar, $maand)
    {
        return Wedstrijd::whereYear('datum', $jaar)
                    ->whereMonth('datum', $maand)
                    ->get();
    }
}
