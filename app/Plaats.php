<?php

namespace Mvh;

use Illuminate\Database\Eloquent\Model;

class Plaats extends Model
{
    protected $table = 'plaatsen';
    protected $fillable = [
        'nummer'
    ];

    public function reeks()
    {
        return $this->belongsTo('Mvh\Reeks');
    }

    public function deelnemers()
    {
        //return $this->hasMany('Mvh\PlaatsDeelnemer', 'plaats_id', 'id');
        return $this->hasMany('Mvh\PlaatsDeelnemer');
    }

    public function gewichten()
    {
        return $this->hasMany('Mvh\PlaatsGewicht');
    }

    public function gewicht()
    {
        return $this->gewichten()->sum('gewicht');
    }
}
