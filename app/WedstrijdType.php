<?php

namespace Mvh;

use Illuminate\Database\Eloquent\Model;

class WedstrijdType extends Model
{
    const DEFAULT_AANTAL_WEDSTRIJDEN = 0;
    const DEFAULT_AANTAL_BESTE_WEDSTRIJDEN = 0;

    protected $table = 'wedstrijdtypes';
    protected $fillable = [
        'omschrijving', 'aantal_wedstrijden', 'aantal_beste_wedstrijden'
    ];

    public function wedstrijden()
    {
        return $this->hasMany('Mvh\Wedstrijd', 'wedstrijdtype_id');   
    }
}
