<?php

namespace Mvh;

use Illuminate\Database\Eloquent\Model;

class ContactPersoon extends Model
{
    protected $table="contactpersonen";
    protected $fillable = ['club_id', 'naam', 'voornaam', 'adres', 'telefoon', 'email'];

    public function club()
    {
        return $this->belongsTo('Mvh\Club')->get();
    }

    public function volledigeNaam()
    {
        return $this->naam . ' ' . $this->voornaam;
    }
}
