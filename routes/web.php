<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Mvh\Club;
use Mvh\Kalender;
use Mvh\Wedstrijd;

Route::get(
    'uitslag/{wedstrijd_id}',
    function($wedstrijd_id) {
        $wedstrijd = Wedstrijd::find($wedstrijd_id);
        $uitslagDetail = $wedstrijd->uitslagDetail();
        $page = 'Uitslag';
        return view(
            'uitslag.show',
            [
                'wedstrijd' => $wedstrijd,
                'uitslagDetail' => $uitslagDetail['details'],
                'aantal_plaatsen' => $uitslagDetail['aantal_plaatsen'],
                'aantal_gewichten' => $uitslagDetail['aantal_gewichten'],
                'totaal' => $uitslagDetail['totaal'],
                'page' => $page
            ]
        );
    }
);

Route::get(
    'kalender/{kalender_id}',
    function ($kalender_id) {
        $kalender = Kalender::find($kalender_id);
        $wedstrijden = $kalender->wedstrijden()->get();
        $extra_detail = true;
        $show_link = false;
        $page = 'Kalender';
        return view(
            'kalender.show',
            compact('kalender', 'wedstrijden', 'extra_detail', 'show_link', 'page')
        );
    }
);

Route::get(
    'kalender',
    function () {
        $kalender = Kalender::recentste();
        $page = 'Kalender';
        if (isset($kalender)) {
            return redirect('kalender/' . $kalender->id);
        } else {
            return view(
                'kalender.show',
                compact('kalender', 'page')
            );
        }
    }
);

Route::get(
    '/',
    function () {
        $wedstrijden = Wedstrijd::inJaarMaand(date('Y'), date('m'));
        $extra_detail = true;
        $show_link = false;
        $page = 'Home';
        return view(
            'home.show',
            compact('wedstrijden', 'extra_detail', 'show_link', 'page')
        );
    }
);

Route::get(
    'uitslagen',
    function () {
        $kalender = Kalender::recentste();
        $wedstrijden = null;
        $extra_detail = false;
        $show_link = true;
        $url = '/uitslag/';
        $page = 'Uitslagen';
        if (isset($kalender)) {
            $wedstrijden = $kalender->wedstrijden()->get();
        }
        return view(
            'uitslagen.show',
            compact('kalender', 'wedstrijden', 'extra_detail', 'show_link', 'page', 'url')
        );
    }
);

Route::get(
    'contact',
    function () {
        $club = Club::first();
        $contactpersonen = $club ? $club->contactPersonen()->get(): null;
        $page = 'Contact';
        return view(
            'club.show',
            compact('club', 'contactpersonen', 'page')
        );
    }
);

Route::get(
    'archief/kalenders',
    function () {
        $kalenders = Kalender::orderBy('jaar', 'desc')->get();
        $page = 'Archief';
        return view(
            'archief.kalenders.show',
            compact('kalenders', 'page')
        );
    }
);

Route::get(
    'archief/kalender/{kalender}',
    function (Kalender $kalender) {
        $wedstrijden = $kalender->wedstrijden()->get();
        $page = 'Archief';
        $extra_detail = true;
        $show_link = true;
        $url = '/archief/uitslag/';
        return view(
            'archief.kalender.show',
            compact('kalender', 'wedstrijden', 'extra_detail', 'show_link', 'page', 'url')
        );
    }
);

Route::get(
    'archief/uitslag/{wedstrijd}',
    function (Wedstrijd $wedstrijd) {
        $wedstrijd = Wedstrijd::find($wedstrijd->id);
        $uitslagDetail = $wedstrijd->uitslagDetail();
        $page = 'Archief';
        return view(
            'archief.uitslag.show',
            [
                'wedstrijd' => $wedstrijd,
                'uitslagDetail' => $uitslagDetail['details'],
                'aantal_plaatsen' => $uitslagDetail['aantal_plaatsen'],
                'aantal_gewichten' => $uitslagDetail['aantal_gewichten'],
                'totaal' => $uitslagDetail['totaal'],
                'page' => $page
            ]
        );
    }
);