var gulp = require('gulp');
var notify = require('gulp-notify');
var phpunit = require('gulp-phpunit');

var unitDir = 'tests/Unit';
var featureDir = 'tests/Feature';

gulp.task('unittests', function () {
    var options = {debug: false, notify: true};
    return gulp.src(unitDir + '/*.php')
            .pipe(phpunit('phpunit ' + unitDir, options));
//            .on('error', notify.onError({
//                title: 'PHPUnit Failed',
//                message: 'One or more tests failed, see the cli for details.'
//            }))
//            .pipe(notify({
//                title: 'PHPUnit Passed',
//                message: 'All tests passed!'
//            }));
});

gulp.task('featuretests', function () {
    var options = {debug: false, notify: true};
    return gulp.src(featureDir + '/*.php')
            .pipe(phpunit('phpunit ' + featureDir, options));
//            .on('error', notify.onError({
//                title: 'PHPUnit Failed',
//                message: 'One or more tests failed, see the cli for details.'
//            }))
//            .pipe(notify({
//                title: 'PHPUnit Passed',
//                message: 'All tests passed!'
//            }));
});

gulp.task(
        'watch',
        function () {
            gulp.watch('app/**/*.php', ['unittests']);
            //gulp.watch('app/**/*.php', ['featureTests']);
        }
);

gulp.task(
        'default',
        ['watch']
        );