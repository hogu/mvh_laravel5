# Visclub Moed & Volharding Herenthout's README

TO DO :

kalender :
    - datum_van
    - datum_tot
    - omschrijving
    - opmerkingen (optioneel)

wedstrijd :
    - kalender_id
    - nummer (uniek)
    - datum (uniek, tussen kalender.datum_van en kalender.datum_tot)
    - omschrijving
    - type_wedstrijd
    - aanvangsuur
    - opmerkingen (optioneel)

reeks :
    - wedsrtrijd_id
    - volgnummer (uniek)
    - aanvang (default 13:30)
    - duur (default 2u)
    - opmerkingen (optioneel)
    - gewicht_zak

plaats :
    - reeks_id
    - nummer
    - deelnemer_id

gewicht :
    - reeks_id
    - nummer
    - gewicht

wedstrijdtype :
    - omschrijving
    - aantal_westrijden
    - beste_van
    
deelnemer :
    - wedsrijd_id
    - persoon_id

persoon :
    - nummer
    - naam (hoofdletters)
    - voornaam (eerste letter = hoofdletter)
    - is lid
