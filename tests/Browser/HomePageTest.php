<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class HomePageTest extends DuskTestCase
{
    use DatabaseMigrations;

    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testGeenWedstrijdenDezeMaand()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertPathIs('/')
                    ->assertSee('Geen wedstrijden/evenementen deze maand !');
        });
    }

    public function testWedstrijdenDezeMaand()
    {
        $this->kalender = bewaarKalender(['jaar' => date('Y')]);
        $this->wedstrijd = bewaarWedstrijd(['kalender_id' => $this->kalender->id, 'datum' => date('Y-m-d')]);

        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->assertPathIs('/')
                ->assertSee($this->wedstrijd->getDatum())
                ->assertSee($this->wedstrijd->omschrijving)
                ->assertSee($this->wedstrijd->getAanvang())
            ;
        });
    }

    public function testHomeLink()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->assertPathIs('/')
                ->assertSeeLink('Home');
        });
    }
}
