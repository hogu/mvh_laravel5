<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ArchiefPageTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function testGeenKalendersBeschikbaar()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->clickLink('Archief')
                ->assertPathIs('/archief/kalenders')
                ->assertSee('Geen kalenders beschikbaar !');
        });
    }

    public function testKalendersBeschikbaar()
    {
        $this->setUpKalender();

        $this->browse(
            function (Browser $browser) {
                $omschrijving = $this->kalender->omschrijving();
                $kalender_id = $this->kalender->id;
                $datum = $this->wedstrijdMetUitslag->getDatum();
                $browser->visit('/')
                    ->clickLink('Archief')
                    ->assertPathIs('/archief/kalenders')
                    ->assertSee('Archief :')
                    ->assertSeeLink($omschrijving)
                    ->clickLink($omschrijving)
                    ->assertPathIs('/archief/kalender/' . $kalender_id)
                    ->assertSee('Kalender Archief :')
                    ->assertSeeLink($datum)
                    ->clickLink($datum)
                    ->assertPathIs('/archief/uitslag/' . $this->wedstrijdMetUitslag->id)
                    ->assertSee('Uitslag Archief :')
                    ->assertSee($this->wedstrijdMetUitslag->omschrijving)
                ;
            }
        );
    }

    public function testArchiefLink()
    {
        $this->setUpKalender();

        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->assertPathIs('/')
                ->assertSeeLink('Archief')
            ;
        });
    }

    private function setUpKalender($metUitslag = true)
    {
        $this->kalender = bewaarKalender(['jaar' => date('Y')]);
        if ($metUitslag) {
            $this->wedstrijd = bewaarWedstrijd(['kalender_id' => $this->kalender->id, 'datum' => date('Y-m-d')]);
            $this->wedstrijdMetUitslag = maakWedstrijdUitslag();
        }
    }

}
