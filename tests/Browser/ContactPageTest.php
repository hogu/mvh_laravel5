<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ContactPageTest extends DuskTestCase
{
    use DatabaseMigrations;

    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testGeenContactGegevens()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('contact')
                    ->assertPathIs('/contact')
                    ->assertSee('Geen contactgegevens beschikbaar !');
        });
    }

    public function testContactGegevensBeschikbaar()
    {
        $this->setUpClub();

        $this->browse(function (Browser $browser) {
            $browser->visit('contact')
                ->assertPathIs('/contact')
            ;
        });
    }

    public function testContactLink()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->assertPathIs('/')
                ->assertSeeLink('Contact');
        });
    }

    private function setUpClub()
    {
        $this->club = bewaarClub();
        $this->eersteContactPersoon = bewaarContactPersoon(['club_id' => $this->club->id]);
        $this->tweedeContactPersoon = bewaarContactPersoon(['club_id' => $this->club->id]);
    }
}
