<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class KalenderPageTest extends DuskTestCase
{
    use DatabaseMigrations;

    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testGeenKalenderBeschikbaar()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/kalender')
                    ->assertPathIs('/kalender')
                    ->assertSee('Geen kalender beschikbaar !');
        });
    }

    public function testKalenderBeschikbaarMetWedstrijden()
    {
        $this->setUpKalender();

        $this->browse(
            function (Browser $browser) {
                $browser->visit('/kalender')
                        ->assertPathIs('/kalender/' . $this->kalender->id)
                        ->assertSee($this->wedstrijd->getDatum())
                        ->assertSee($this->wedstrijd->omschrijving)
                        ->assertSee($this->wedstrijd->getAanvang())
                ;
            }
        );
    }

    public function testKalenderBeschikbaarZonderWedstrijden()
    {
        $this->setUpKalender(false);

        $this->browse(
            function (Browser $browser) {
                $browser->visit('/kalender')
                        ->assertPathIs('/kalender/' . $this->kalender->id)
                        ->assertSee('Geen wedstrijden beschikbaar !')
                ;
            }
        );
    }

    public function testKalenderLink()
    {
        $this->setUpKalender();

        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->assertPathIs('/')
                ->assertSeeLink('Kalender')
                ->clickLink('Kalender')
                ->assertPathIs('/kalender/' . $this->kalender->id)
                ->assertSee($this->wedstrijd->getDatum())
                ->assertSee($this->wedstrijd->omschrijving)
                ->assertSee($this->wedstrijd->getAanvang());
        });
    }

    private function setUpKalender($metKalender = true)
    {
        $this->kalender = bewaarKalender(['jaar' => date('Y')]);
        if ($metKalender) {
            $this->wedstrijd = bewaarWedstrijd(['kalender_id' => $this->kalender->id, 'datum' => date('Y-m-d')]);
        }
    }
}
