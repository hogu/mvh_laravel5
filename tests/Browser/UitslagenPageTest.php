<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UitslagenPageTest extends DuskTestCase
{
    use DatabaseMigrations;

    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testGeenUitslagenBeschikbaar()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/uitslagen')
                    ->assertPathIs('/uitslagen')
                    ->assertSee('Geen uitslagen beschikbaar !');
        });
    }

    public function testUitslagenBeschikbaar()
    {
        $this->setUpKalender();

        $this->browse(function (Browser $browser) {
            $browser->visit('/uitslagen')
                ->assertPathIs('/uitslagen')
                ->assertSee($this->wedstrijd->getDatum())
                ->assertSee($this->wedstrijd->omschrijving);
        });
    }

    public function testUitslagenLink()
    {
        $this->setUpKalender();

        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->assertPathIs('/')
                ->assertSeeLink('Uitslagen')
                ->clickLink('Uitslagen')
                ->assertPathIs('/uitslagen')
                ->assertSee($this->wedstrijd->getDatum())
                ->assertSee($this->wedstrijd->omschrijving);
        });
    }

    public function testUitslagDetail()
    {
        $this->setUpKalender();

        $this->browse(
            function (Browser $browser) {
                $wedstrijdDatum = $this->wedstrijdMetUitslag->getDatum();
                $uitslagPath = '/uitslag/' . $this->wedstrijdMetUitslag->id;
                $browser->visit('/')
                    ->assertPathIs('/')
                    ->assertSeeLink('Uitslagen')
                    ->clickLink('Uitslagen')
                    ->assertPathIs('/uitslagen')
                    ->assertSeeLink($wedstrijdDatum)
                    ->clickLink($wedstrijdDatum)
                    ->assertPathIs($uitslagPath);
            }
        );
    }

    public function testGeenUitslagDetail()
    {
        $this->setUpKalender(false);

        $this->browse(
            function (Browser $browser) {
                $browser->visit('/')
                    ->assertPathIs('/')
                    ->assertSeeLink('Uitslagen')
                    ->clickLink('Uitslagen')
                    ->assertPathIs('/uitslagen')
                    ->assertSee('Geen wedstrijden beschikbaar !')
                ;
            }
        );
    }

    private function setUpKalender($metUitslag = true)
    {
        $this->kalender = bewaarKalender(['jaar' => date('Y')]);
        if ($metUitslag) {
            $this->wedstrijd = bewaarWedstrijd(['kalender_id' => $this->kalender->id, 'datum' => date('Y-m-d')]);
            $this->wedstrijdMetUitslag = maakWedstrijdUitslag();
        }
    }
}
