<?php

namespace Tests\Unit;

use Mvh\WedstrijdType;
use Tests\TestCase;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WedstrijdDeelnemerTest extends TestCase
{
    private $wedstrijd;
    private $wedstrijd_id;
    private $deelnemer;
    private $deelnemer_id;

    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();
        $this->wedstrijd = bewaarWedstrijd();
        $this->wedstrijd_id = $this->wedstrijd->id;
        $this->deelnemer = bewaarDeelnemer();
        $this->deelnemer_id = $this->deelnemer->id;
    }

    /** @test */
    public function heeftEenWedstrijdId()
    {
        $wedstrijdDeelnemer = bewaarWedstrijdDeelnemer(['wedstrijd_id' => $this->wedstrijd_id]);

        $this->assertEquals($this->wedstrijd_id, $wedstrijdDeelnemer->wedstrijd_id);
    }

    /** @test */
    public function heeftEenDeelnemerId()
    {
        $wedstrijdDeelnemer = bewaarWedstrijdDeelnemer(
            ['wedstrijd_id' => $this->wedstrijd_id, 'deelnemer_id' => $this->deelnemer_id]
        );

        $this->assertEquals($this->deelnemer_id, $wedstrijdDeelnemer->deelnemer_id);
    }

    /** @test */
    public function wedstrijdDeelnemerIsUniek()
    {
        $this->expectException(QueryException::class);

        $wedstrijdDeelnemer = bewaarWedstrijdDeelnemer(
            ['wedstrijd_id' => $this->wedstrijd_id, 'deelnemer_id' => $this->deelnemer_id]
        );
        $wedstrijdDeelnemer = bewaarWedstrijdDeelnemer(
            ['wedstrijd_id' => $this->wedstrijd_id, 'deelnemer_id' => $this->deelnemer_id]
        );
    }

    /** @test */
    public function heeftEenWedstrijd()
    {
        $wedstrijdDeelnemer = bewaarWedstrijdDeelnemer(['wedstrijd_id' => $this->wedstrijd_id]);

        $actualWedstrijd = $wedstrijdDeelnemer->wedstrijd()->get();

        $this->assertCount(1, $actualWedstrijd);
        $this->assertEquals($this->wedstrijd_id, $actualWedstrijd->first()->id);
        $this->assertEquals($this->wedstrijd->omschrijving, $actualWedstrijd->first()->omschrijving);
    }

    /** @test */
    public function heeftEenDeelnemer()
    {
        $wedstrijdDeelnemer = bewaarWedstrijdDeelnemer(
            ['wedstrijd_id' => $this->wedstrijd_id, 'deelnemer_id' => $this->deelnemer_id]
        );

        $actualDeelnemer = $wedstrijdDeelnemer->deelnemer()->get();

        $this->assertCount(1, $actualDeelnemer);
        $this->assertEquals($this->deelnemer_id, $actualDeelnemer->first()->id);
        $this->assertEquals($this->deelnemer->naam, $actualDeelnemer->first()->naam);
        $this->assertEquals($this->deelnemer->voornaam, $actualDeelnemer->first()->voornaam);
    }
}

