<?php

namespace Tests\Unit;

use Mvh\WedstrijdType;
use Tests\TestCase;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WedstrijdTypeTest extends TestCase
{
    use DatabaseTransactions;
    
    /** @test */
    public function heeftEenOmschrijving()
    {
        $omschrijving = 'OMSCHRIJVING';

        $wedstrijdType = bewaarWedstrijdType(['omschrijving' => $omschrijving]);

        $this->assertEquals($omschrijving, $wedstrijdType->omschrijving);
    }
    
    /** @test */
    public function heeftAantalWedstrijden()
    {
        $aantalWedstrijden = 1;

        $wedstrijdType = bewaarWedstrijdType(['aantal_wedstrijden' => $aantalWedstrijden]);

        $this->assertEquals($aantalWedstrijden, $wedstrijdType->aantal_wedstrijden);
    }

    /** @test */
    public function aantalWedstrijdenHeeftEenDefaultWaarde()
    {
        $wedstrijdType = new WedstrijdType(
            ['omschrijving' => 'OMSCHRIJVING', 'aantal_beste_wedstrijden' => 5]
        );
        $wedstrijdType->save();
        $actualWedstrijdType = WedstrijdType::find($wedstrijdType->id);

        $this->assertEquals(WedstrijdType::DEFAULT_AANTAL_WEDSTRIJDEN, $actualWedstrijdType->aantal_wedstrijden);
    }

    /** @test */
    public function heeftAantalBesteWedstrijden()
    {
        $aantalBesteWedstrijden = 1;

        $wedstrijdType = bewaarWedstrijdType(['aantal_beste_wedstrijden' => $aantalBesteWedstrijden]);

        $this->assertEquals($aantalBesteWedstrijden, $wedstrijdType->aantal_beste_wedstrijden);
    }

    /** @test */
    public function aantalBesteWedstrijdenHeeftEenDefaultWaarde()
    {
        $wedstrijdType = new WedstrijdType(
            ['omschrijving' => 'OMSCHRIJVING', 'aantal_wedstrijden' => 5]
        );
        $wedstrijdType->save();
        $actualWedstrijdType = WedstrijdType::find($wedstrijdType->id);

        $this->assertEquals(WedstrijdType::DEFAULT_AANTAL_BESTE_WEDSTRIJDEN, $actualWedstrijdType->aantal_beste_wedstrijden);
    }

    /** @test */
    public function heeftWedstrijden()
    {
        $wedstrijdType = bewaarWedstrijdType();
        $eersteWedstrijd = maakWedstrijd(['wedstrijdtype_id' => $wedstrijdType->id]);
        $tweedeWedstrijd = maakWedstrijd(['wedstrijdtype_id' => $wedstrijdType->id]);

        $wedstrijdType->wedstrijden()->save($eersteWedstrijd);
        $wedstrijdType->wedstrijden()->save($tweedeWedstrijd);

        $wedstrijden = $wedstrijdType->wedstrijden()->get();

        $this->assertCount(2, $wedstrijden);
        $this->assertEquals($eersteWedstrijd->id, $wedstrijden->first()->id);
        $this->assertEquals($tweedeWedstrijd->id, $wedstrijden->last()->id);
    }
}

