<?php

namespace Tests\Unit;

use Carbon\Carbon;
use Mvh\Kalender;
use Mvh\Wedstrijd;
use Mvh\Reeks;
use Tests\TestCase;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class KalenderTest extends TestCase
{
    private $vandaag;

    use DatabaseTransactions;

    public function __construct()
    {
        parent::__construct();

        $nu = Carbon::now();
        $this->vandaag = $nu->format('Y-m-d');
    }

    /** @test */
    public function heeftEenJaar()
    {
        $jaar = date('Y');

        $kalender = new Kalender(['jaar' => $jaar]);

        $this->assertEquals($jaar, $kalender->jaar);
    }

    /** @test */
    public function heeftOpmerkingen()
    {
        $opmerkingen = 'OPMERKING';

        $kalender = new Kalender(['opmerkingen' => $opmerkingen]);

        $this->assertEquals($opmerkingen, $kalender->opmerkingen);
    }

    /** @test */
    public function opmerkingenIsOptioneel()
    {
        $kalender = bewaarKalender(
            ['jaar' => date('Y'),'opmerkingen' => null]
        );

        $this->assertNull($kalender->opmerkingen);
    }

    /** @test */
    public function heeftWedstrijden()
    {
        $kalender = bewaarKalender();
        $expectedWedstrijd = maakWedstrijd();
        $kalender->wedstrijden()->save($expectedWedstrijd);

        $actualWedstrijden = $kalender->wedstrijden()->get();
        $actualWedstrijd = $actualWedstrijden->first();

        $this->assertCount(1, $actualWedstrijden);
        $this->assertEquals($expectedWedstrijd->id, $actualWedstrijd->id);
        $this->assertEquals($expectedWedstrijd->kalender_id, $actualWedstrijd->kalender_id);
        $this->assertEquals($expectedWedstrijd->datum, $actualWedstrijd->datum);
        $this->assertEquals($expectedWedstrijd->omschrijving, $actualWedstrijd->omschrijving);
        $this->assertEquals($expectedWedstrijd->aanvang, $actualWedstrijd->aanvang);
        $this->assertEquals($expectedWedstrijd->opmerkingen, $actualWedstrijd->opmerkingen);
        $this->assertEquals($expectedWedstrijd->wedstrijdtype_id, $actualWedstrijd->wedstrijdtype_id);
    }

    /** @test */
    public function heeftEenOmschrijving()
    {
        $kalender = bewaarKalender();
        $expectedOmschrijving = 'Wedstrijdkalender ' . $kalender->jaar;

        $actualOmschrijving = Kalender::find($kalender->id)->omschrijving();

        $this->assertEquals($expectedOmschrijving, $actualOmschrijving);
    }

    /** @test */
    public function geeftDeRecentsteKalenderTerug()
    {
        $kalender1 = bewaarKalender(['jaar' => '2016']);
        $kalender2 = bewaarKalender(['jaar' => '2015']);

        $recentsteKalender = Kalender::recentste();

        $this->assertEquals($kalender1->id, $recentsteKalender->id);
    }
}
