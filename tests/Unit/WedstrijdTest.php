<?php

namespace Tests\Unit;

use Carbon\Carbon;
use Mvh\Wedstrijd;
use Mvh\Reeks;
use Tests\TestCase;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WedstrijdTest extends TestCase
{
    private $vandaag;
    private $tijd;

    use DatabaseTransactions;

    public function __construct()
    {
        parent::__construct();

        $nu = Carbon::now();
        $this->vandaag = $nu->format('Y-m-d');
        $this->aanvang = $nu->format('H:m');
    }


    /** @test */
    public function eenWedstrijdHeeftEenOmschrijving()
    {
        $omschrijving = 'OMSCHRIJVING';

        $wedstrijd = new Wedstrijd(['omschrijving' => $omschrijving]);

        $this->assertEquals($omschrijving, $wedstrijd->omschrijving);
    }

    /** @test */
    public function eenWedstrijdHeeftEenDatum()
    {
        $wedstrijd = new Wedstrijd(['datum' => $this->vandaag]);

        $this->assertEquals($this->vandaag, $wedstrijd->datum);
    }

    /** @test */
    public function eenWedstrijdHeeftEenUniekeDatum()
    {
        $this->expectException(QueryException::class);

        $eersteWedstrijd = bewaarWedstrijd(['datum' => $this->vandaag]);
        $tweedeWedstrijd = bewaarWedstrijd(['datum' => $this->vandaag]);
    }

    /** @test */
    public function eenDatumKanOpgevraagdWordenInFormaatDagMaandJaar()
    {
        $datum = Carbon::now();
        $wedstrijd = maakWedstrijd(['datum' => $datum]);

        $this->assertEquals($datum->format('d/m/Y'), $wedstrijd->getDatum());
    }
    /** @test */
    public function eenWedstrijdHeeftEenAanvang()
    {
        $wedstrijd = maakWedstrijd(['aanvang' => $this->aanvang]);

        $this->assertEquals($this->aanvang, $wedstrijd->aanvang);
    }

    /** @test */
    public function eenAanvangHeeftEenDefaultWaarde()
    {
        $wedstrijdType = bewaarWedstrijdType();
        $kalender = bewaarKalender();
        $wedstrijd = new Wedstrijd(
            ['wedstrijdtype_id' => $wedstrijdType->id, 'omschrijving' => 'omschrijving', 'datum' => $this->vandaag, 'kalender_id' => $kalender->id]
        );
        $wedstrijd->save();
        $actualWedstrijd = Wedstrijd::find($wedstrijd->id);

        $this->assertEquals(Wedstrijd::DEFAULT_AANVANG, $actualWedstrijd->aanvang);
    }

    /** @test */
    public function eenAanvangKanOpgevraagdWordenInFormaatUurMinuten()
    {
        $aanvang = Carbon::now()->format('H:m:s');
        $wedstrijd = maakWedstrijd(['aanvang' => $aanvang]);

        $this->assertEquals(substr($aanvang, 0, 5), $wedstrijd->getAanvang());
    }

    /** @test */
    public function eenWedstrijdHeeftOpmerkingen()
    {
        $opmerkingen = 'OPMERKING';

        $wedstrijd = new Wedstrijd(['opmerkingen' => $opmerkingen]);

        $this->assertEquals($opmerkingen, $wedstrijd->opmerkingen);
    }

    /** @test */
    public function opmerkingenIsOptioneel()
    {
        $omschrijving = 'OMSCHRIJVING';
        $wedstrijd = maakWedstrijd(['opmerkingen' => null]);

        $wedstrijd->save();

        $this->assertNull($wedstrijd->opmerkingen);
    }

    /** @test */
    public function eenWedstrijdHeeftReeksen()
    {
        $wedstrijd = bewaarWedstrijd();
        $excpectedReeks = maakReeks();
        $wedstrijd->reeksen()->save($excpectedReeks);

        $actualReeksen = $wedstrijd->reeksen()->get();

        $this->assertCount(1, $actualReeksen);
        $this->assertEquals($excpectedReeks->id, $actualReeksen->first()->id);
    }

    /** @test */
    public function eenWedstrijdHeeftEenType()
    {
        $wedstrijd = maakWedstrijd();
        $expectedWedtrijdType = bewaarWedstrijdType();
        $wedstrijd->wedstrijdtype_id = $expectedWedtrijdType->id;
        $wedstrijd->save();

        $actualWedstrijdType = $wedstrijd->type()->get();
        
        $this->assertCount(1, $actualWedstrijdType);
        $this->assertEquals($expectedWedtrijdType->id, $actualWedstrijdType->first()->id);
    }

    /** @test */
    public function heeftDeelnemers()
    {
        $deelnemer1 = bewaarDeelnemer(['nummer' => 1]);
        $deelnemer2 = bewaarDeelnemer(['nummer' => 2]);
        $wedstrijd = bewaarWedstrijd();
        $wedstrijdDeelnemer1 = maakWedstrijdDeelnemer(
            ['wedstrijd_id' => $wedstrijd->id, 'deelnemer_id' => $deelnemer1->id]
        );
        $wedstrijdDeelnemer2 = maakWedstrijdDeelnemer(
            ['wedstrijd_id' => $wedstrijd->id, 'deelnemer_id' => $deelnemer2->id]
        );

        $wedstrijd->deelnemers()->save($wedstrijdDeelnemer1);
        $wedstrijd->deelnemers()->save($wedstrijdDeelnemer2);

        $actualDeelnemers = $wedstrijd->deelnemers()->get();

        $this->assertCount(2, $actualDeelnemers);
        $this->assertEquals($wedstrijdDeelnemer1->id, $actualDeelnemers->first()->id);
        $this->assertEquals($wedstrijdDeelnemer2->id, $actualDeelnemers->last()->id);
    }

    /** @test */
    public function heeftUitslagDetail()
    {
        $expectedDetails = [
            json_decode(
                '{"volgnummer": 1, "deelnemers": "NAAM2 Voornaam2", "plaatsen": [2, "", ""], "gewichten": [200, 200, 200], "totaal": "600"}',
                true
            ),
            json_decode(
                '{"volgnummer": 2, "deelnemers": "NAAM1 Voornaam1", "plaatsen": [1, "", ""], "gewichten": [100, 100, 100], "totaal": "300"}',
                true
            )
        ];

        $wedstrijd = maakWedstrijdUitslag(2);

        $uitslagDetail = $wedstrijd->uitslagDetail();

        $this->assertCount(2, $uitslagDetail['details']);
        $this->assertEquals($uitslagDetail['totaal'], 900);
        $this->assertEquals($expectedDetails[0], $uitslagDetail['details'][0]);
        $this->assertEquals($expectedDetails[1], $uitslagDetail['details'][1]);
    }

    /** @test */
    public function heeftKoppelUitslagDetail()
    {
        $expectedDetails = [
            json_decode(
                '{"volgnummer": 1, "deelnemers": "NAAM3 Voornaam3 - NAAM4 Voornaam4", "plaatsen": [2, "", ""], "gewichten": [200, 200, 200], "totaal": "600"}',
                true
            ),
            json_decode(
                '{"volgnummer": 2, "deelnemers": "NAAM1 Voornaam1 - NAAM2 Voornaam2", "plaatsen": [1, "", ""], "gewichten": [100, 100, 100], "totaal": "300"}',
                true
            )
        ];

        $wedstrijd = maakKoppelUitslag(4);

        $uitslagDetail = $wedstrijd->uitslagDetail();

        $this->assertCount(2, $uitslagDetail['details']);
        $this->assertEquals($uitslagDetail['totaal'], 900);
        $this->assertEquals($expectedDetails[0], $uitslagDetail['details'][0]);
        $this->assertEquals($expectedDetails[1], $uitslagDetail['details'][1]);
    }

    /** @test */
    public function heeftMarathonUitslagDetail()
    {
        $expectedDetails = [
            json_decode(
                '{"volgnummer": 1, "deelnemers": "NAAM1 Voornaam1", "plaatsen": [1, 2, ""], "gewichten": [100, 200, 200], "totaal": "500"}',
                true
            ),
            json_decode(
                '{"volgnummer": 2, "deelnemers": "NAAM2 Voornaam2", "plaatsen": [2, 1, ""], "gewichten": [200, 100, 100], "totaal": "400"}',
                true
            )
        ];

        $wedstrijd = maakMarathonUitslag(2);

        $uitslagDetail = $wedstrijd->uitslagDetail();

        $this->assertCount(2, $uitslagDetail['details']);
        $this->assertEquals($uitslagDetail['totaal'], 900);
        $this->assertEquals($expectedDetails[0], $uitslagDetail['details'][0]);
        $this->assertEquals($expectedDetails[1], $uitslagDetail['details'][1]);
    }

    /** @test */
    public function heeftEenKalenderId()
    {
        $kalender = bewaarKalender();
        $wedstrijd = new Wedstrijd(['kalender_id' => $kalender->id]);

        $this->assertEquals($kalender->id, $wedstrijd->kalender_id);
    }

    /** @test */
    public function heeftEenKalender()
    {
        $expectedKalender = bewaarKalender();
        $wedstrijd = bewaarWedstrijd(['kalender_id' => $expectedKalender->id]);

        $actualKalender = $wedstrijd->kalender()->first();

        $this->assertEquals($expectedKalender->id, $actualKalender->id);
        $this->assertEquals($expectedKalender->jaar, $actualKalender->jaar);
        $this->assertEquals($expectedKalender->opmerkingen, $actualKalender->opmerkingen);
    }

    /** @test */
    public function wedstrijdeninJaarMaand()
    {
        $jaar = date('Y');
        $huidigeMaand = vandaag()->format('Y-m-d');
        $vorigeMaand = vandaag()->subDays(30)->format('Y-m-d');
        $volgendeMaand = vandaag()->addDays(30)->format('Y-m-d');
        $maand = date('m');

        $kalender = bewaarKalender(['jaar' => $jaar]);
        $vorigeWedstrijd = bewaarWedstrijd(
            ['kalender_id' => $kalender->id, 'datum' => $vorigeMaand]);
        $huidigeWedstrijd = bewaarWedstrijd(
            ['kalender_id' => $kalender->id, 'datum' => $huidigeMaand]);
        $volgendeWedstrijd = bewaarWedstrijd(
            ['kalender_id' => $kalender->id, 'datum' => $volgendeMaand]);

        $wedstrijden = Wedstrijd::inJaarMaand($jaar, $maand);
        $wedstrijd = $wedstrijden->first();

        $this->assertCount(1, $wedstrijden);
        $this->assertEquals($huidigeWedstrijd->kalender_id, $wedstrijd->kalender_id);
        $this->assertEquals($huidigeWedstrijd->id, $wedstrijd->id);
        $this->assertEquals($huidigeWedstrijd->datum, $wedstrijd->datum);
        $this->assertEquals($huidigeWedstrijd->omschrijving, $wedstrijd->omschrijving);
        $this->assertEquals($huidigeWedstrijd->opmerkingen, $wedstrijd->opmerkingen);
    }

    /** @test */
    public function heeftEenTotaalGewicht()
    {
        $kalender = bewaarKalender();
        $wedstrijd = bewaarWedstrijd(['kalender_id' => $kalender->id]);

        $totaalGewicht = $wedstrijd->totaalGewicht();

        $this->assertEquals(0, $totaalGewicht);
    }
}
