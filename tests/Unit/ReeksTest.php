<?php

namespace Tests\Unit;

use Carbon\Carbon;
use Mvh\Reeks;
use Mvh\Wedstrijd;
use Tests\TestCase;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReeksTest extends TestCase
{
    private $wedstrijd;
    
    use DatabaseTransactions;
    
    public function setUp() {
        parent::setUp();
        $this->wedstrijd = bewaarWedstrijd();
    }
    /** @test */
    public function eenReeksHeeftAanvang()
    {
        $aanvang = '13:30';

        $reeks = new Reeks(['aanvang' => $aanvang, 'wedstrijd_id' => 1]);

        $this->assertEquals($aanvang, $reeks->aanvang);
    }

    /** @test */
    public function aanvangHeeftEenDefaultWaarde()
    {
        $reeks = new Reeks(['wedstrijd_id' => $this->wedstrijd->id]);
        $reeks->save();
        $actualReeks = Reeks::find($reeks->id);

        $this->assertEquals(Reeks::DEFAULT_AANVANG, $actualReeks->aanvang);
    }

    /** @test */
    public function anvangKanOpgevraagdWordenInFormaatUurMinuten()
    {
        $aanvang = Carbon::now()->format('H:m:s');
        $wedstrijd = maakReeks(['aanvang' => $aanvang]);

        $this->assertEquals(substr($aanvang, 0, 5), $wedstrijd->getAanvang());
    }

    /** @test */
    public function eenReeksHeeftEenDuur()
    {
        $duur = Reeks::DEFAULT_DUUR;

        $reeks = new Reeks(['duur' => $duur, 'wedstrijd_id' => $this->wedstrijd->id]);

        $this->assertEquals($duur, $reeks->duur);
    }

    /** @test */
    public function duurHeeftEenDefaultWaarde()
    {
        $reeks = new Reeks(['wedstrijd_id' => $this->wedstrijd->id]);
        $reeks->save();
        $actualReeks = Reeks::find($reeks->id);

        $this->assertEquals(Reeks::DEFAULT_DUUR, $actualReeks->duur);
    }

    /** @test */
    public function duurKanOpgevraagdWordenInFormaatUurMinuten()
    {
        $duur = Carbon::now()->format('H:m:s');
        $wedstrijd = maakReeks(['duur' => $duur]);

        $this->assertEquals(substr($duur, 0, 5), $wedstrijd->getDuur());
    }

    /** @test */
    public function eenReeksHeeftOpmerkingen()
    {
        $opmerkingen = 'OPMERKINGEN';

        $reeks = new Reeks(
            ['opmerkingen' => $opmerkingen, 'wedstrijd_id' => $this->wedstrijd->id]
        );

        $this->assertEquals($opmerkingen, $reeks->opmerkingen);
    }

    /** @test */
    public function opmerkingenIsOptioneel()
    {
        $reeks = new Reeks(['wedstrijd_id' => $this->wedstrijd->id]);
        $reeks->save();
        $actualReeks = Reeks::find($reeks->id);

        $this->assertNull($actualReeks->opmerkingen);
    }

    /** @test */
    public function eenReeksHeeftEenZakGewicht()
    {
        $zakgewicht = 1120;

        $reeks = new Reeks(
            ['zakgewicht' => $zakgewicht, 'wedstrijd_id' => $this->wedstrijd->id]
        );

        $this->assertEquals($zakgewicht, $reeks->zakgewicht);
    }

    /** @test */
    public function zakgewichtHeeftEenDefaultWaarde()
    {
        $reeks = new Reeks(
            ['wedstrijd_id' => $this->wedstrijd->id]
        );
        $reeks->save();
        $actualReeks = Reeks::find($reeks->id);

        $this->assertEquals(Reeks::DEFAULT_ZAKGEWICHT, $actualReeks->zakgewicht);
    }

    /** @test */
    public function zakgewichtNietNegatief()
    {
        $this->expectException(QueryException::class);

        bewaarReeks(['zakgewicht' => -1]);
    }

    /** @test */
    public function eenReeksHeeftEenWedstrijd()
    {
        $reeks = maakReeks();

        $this->wedstrijd->reeksen()->save($reeks);
        $wedstrijden = $reeks->wedstrijd();

        $this->assertEquals(1,$wedstrijden->count());
        $this->assertEquals($reeks->wedstrijd()->first()->id, $this->wedstrijd->id);
    }

    /** @test */
    public function heeftEenVolgnummer()
    {
        $volgnummer = 1;

        $reeks = bewaarReeks(
            ['volgnummer' => $volgnummer, 'wedstrijd_id' => $this->wedstrijd->id]
        );

        $this->assertEquals($volgnummer, $reeks->volgnummer);
    }

    /** @test */
    public function volgnummerHeeftDefaultWaarde()
    {
        $reeks = new Reeks(
            ['wedstrijd_id' => $this->wedstrijd->id]
        );
        $reeks->save();
        $actualReeks = Reeks::find($reeks->id);

        $this->assertEquals(Reeks::DEFAULT_VOLGNUMMER, $actualReeks->volgnummer);

    }

    /** @test */
    public function volgnummerNietNegatief()
    {
        $this->expectException(QueryException::class);

        bewaarReeks(
            ['volgnummer' => -1, 'wedstrijd_id' => $this->wedstrijd->id]
        );
    }

    /** @test */
    public function volgnummerIsUniek()
    {
        $this->expectException(QueryException::class);

        bewaarReeks(
            ['wedstrijd_id' => $this->wedstrijd->id]
        );
        bewaarReeks(
            ['wedstrijd_id' => $this->wedstrijd->id]
        );
    }

    /** @test */
    public function heeftPlaatsen()
    {
        $plaats = maakPlaats();
        $reeks = bewaarReeks(['wedstrijd_id' => $this->wedstrijd->id]);
        $reeks->plaatsen()->save($plaats);

        $plaatsen = $reeks->plaatsen()->get();

        $this->assertCount(1, $plaatsen);
        $this->assertEquals($plaats->id, $plaatsen->first()->id);
    }
}
