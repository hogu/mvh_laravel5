<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mvh\Club;
use Tests\TestCase;

class ClubTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function heeftEenNaam()
    {
        $naam = 'CLUBNAAM';

        $club = new Club(['naam' => $naam]);

        $this->assertEquals($naam, $club->naam);
    }

    /** @test */
    public function heeftEenAdres()
    {
        $adres = 'STRAAT + NUMMER' . chr(10) . 'POSTCODE + GEMEENTE';

        $club = new Club(['adres' => $adres]);

        $this->assertEquals($adres, $club->adres);
    }

    /** @test */
    public function heeftEenTelefoon()
    {
        $telefoon = '014 / 51 33 77';

        $club = new Club(['telefoon' => $telefoon]);

        $this->assertEquals($telefoon, $club->telefoon);
    }

    /** @test */
    public function heeftEenEmailAdres()
    {
        $email = 'email@hotmail.com';

        $club = new Club(['email' => $email]);

        $this->assertEquals($email, $club->email);
    }

    /** @test */
    public function heeftContactPersonen()
    {
        $club = bewaarClub();
        $contactPersoon = maakContactPersoon();
        $club->contactPersonen()->save($contactPersoon);

        $contactPersonen = $club->contactPersonen()->get();
        $actualContactPersoon = $contactPersonen->first();

        $this->assertCount(1, $contactPersonen);
        $this->assertEquals($contactPersoon->id, $actualContactPersoon->id);
    }
}
