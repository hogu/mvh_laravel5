<?php

namespace Tests\Unit;

use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class PlaatsDeelnemerTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function heeftEenPlaatsId()
    {
        $plaats_id = 1;

        $plaatsDeelnemer = bewaarPlaatsDeelnemer(['plaats_id' => $plaats_id]);

        $this->assertEquals($plaats_id, $plaatsDeelnemer->plaats_id);
    }

    /** @test */
    public function heeftEenWedstrijdDeelnemerId()
    {
        $wedstrijd_deelnemer_id = 1;

        $plaatsDeelnemer = bewaarPlaatsDeelnemer(['wedstrijd_deelnemer_id' => $wedstrijd_deelnemer_id]);

        $this->assertEquals($wedstrijd_deelnemer_id, $plaatsDeelnemer->wedstrijd_deelnemer_id);
    }

    /** @test */
    public function plaatsDeelnemerIsUniek()
    {
        $this->expectException(QueryException::class);

        $plaatsDeelnemer = bewaarPlaatsDeelnemer();
        $plaatsDeelnemer = bewaarPlaatsDeelnemer();
    }
}

