<?php

namespace Tests\Unit;

use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class PlaatsTest extends TestCase
{
    private $wedstrijd;
    private $reeks;

    public function setUp()
    {
        parent::setUp();
        $this->wedstrijd = bewaarWedstrijd();
        $this->reeks = bewaarReeks(['wedstrijd_id' => $this->wedstrijd->id]);
    }

    use DatabaseTransactions;

    /** @test */
    public function heeftEenNummer()
    {
        $nummer = 1;

        $plaats = bewaarPlaats(['nummer' => $nummer]);

        $this->assertEquals($nummer, $plaats->nummer);
    }

    /** @test */
    public function nummerIsUniek()
    {
        $nummer = 1;
        $reeks_id = $this->reeks->id;

        $this->expectException(QueryException::class);

        bewaarPlaats(
            ['nummer' => $nummer, 'reeks_id' => $reeks_id]
        );
        bewaarPlaats(
            ['nummer' => $nummer, 'reeks_id' => $reeks_id]
        );
    }

    /** @test */
    public function heeftEenReeks()
    {
        $plaats = bewaarPlaats(['reeks_id' => $this->reeks->id]);

        $actualReeks = $plaats->reeks()->get();

        $this->assertCount(1, $actualReeks);
        $this->assertEquals($this->reeks->id, $actualReeks->first()->id);
    }

    /** @test */
    public function heeftDeelnemers()
    {
        $plaats = bewaarPlaats(['reeks_id' => $this->reeks->id]);
        $eerstePlaatsDeelnemer = $this->maakDeelnemer($plaats->id, 1);
        $tweedePlaatsDeelnemer = $this->maakDeelnemer($plaats->id, 2);

        $plaats->deelnemers()->save($eerstePlaatsDeelnemer);
        $plaats->deelnemers()->save($tweedePlaatsDeelnemer);

        $actualDeelnemers = $plaats->deelnemers()->get();

        $this->assertCount(2, $actualDeelnemers);
        $this->assertEquals($eerstePlaatsDeelnemer->id, $actualDeelnemers->first()->id);
        $this->assertEquals($tweedePlaatsDeelnemer->id, $actualDeelnemers->last()->id);
    }

    /** @test */
    public function heeftGewichten()
    {
        $plaats = bewaarPlaats(['reeks_id' => $this->reeks->id]);
        $eerstePlaatsGewicht = $this->maakGewicht($plaats->id, 50);
        $tweedePlaatsGewicht = $this->maakGewicht($plaats->id, 50);
        $plaats->gewichten()->save($eerstePlaatsGewicht);
        $plaats->gewichten()->save($tweedePlaatsGewicht);

        $actualGewichten = $plaats->gewichten()->get();

        $this->assertCount(2, $actualGewichten);
        $this->assertEquals($eerstePlaatsGewicht->gewicht, $actualGewichten->first()->gewicht);
        $this->assertEquals($tweedePlaatsGewicht->gewicht, $actualGewichten->last()->gewicht);
    }

    /** @test */
    public function heeftEenGewicht()
    {
        $plaats = bewaarPlaats(['reeks_id' => $this->reeks->id]);
        $eerstePlaatsGewicht = $this->maakGewicht($plaats->id, 50);
        $tweedePlaatsGewicht = $this->maakGewicht($plaats->id, 50);
        $plaats->gewichten()->save($eerstePlaatsGewicht);
        $plaats->gewichten()->save($tweedePlaatsGewicht);

        $actualGewicht = $plaats->gewicht();

        $this->assertEquals(100, $actualGewicht);
    }

    private function maakDeelnemer($plaats_id, $deelnemerNummer)
    {
        $deelnemer = bewaarDeelnemer(['nummer' => $deelnemerNummer]);
        $wedstrijdDeelnemer = bewaarWedstrijdDeelnemer(
            ['wedstrijd_id' => $this->wedstrijd->id, 'deelnemer_id' => $deelnemer->id]
        );
        $plaatsDeelnemer = maakPlaatsDeelnemer(
            ['plaats_id' => $plaats_id, 'wedstrijd_deelnemer_id' => $wedstrijdDeelnemer->id]
        );
        return $plaatsDeelnemer;
    }

    private function maakGewicht($plaats_id, $gewicht)
    {
        return maakPlaatsGewicht(['plaats_id' => $plaats_id, 'gewicht' => $gewicht]);
    }
}
