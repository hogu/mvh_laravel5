<?php

namespace Tests\Unit;

use Mvh\WedstrijdType;
use Tests\TestCase;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DeelnemerTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function heeftEenNummer()
    {
        $nummer = 1;

        $deelnemer = bewaarDeelnemer(['nummer' => $nummer]);

        $this->assertEquals($nummer, $deelnemer->nummer);
    }

    /** @test */
    public function heeftEenUniekNummer()
    {
        $this->expectException(QueryException::class);

        $deelnemer = bewaarDeelnemer(['nummer' => 1]);
        $deelnemer = bewaarDeelnemer(['nummer' => 1]);
    }

    /** @test */
    public function heeftEenNaam()
    {
        $naam = 'Van Hoof';

        $deelnemer = bewaarDeelnemer(['naam' => $naam]);

        $this->assertEquals(strtoupper($naam), $deelnemer->naam);
    }

    /** @test */
    public function heeftEenNaamInHoofdletters()
    {
        $naam = 'van hoof';

        $deelnemer = bewaarDeelnemer(['naam' => $naam]);

        $this->assertEquals(strtoupper($naam), $deelnemer->naam);
    }

    /** @test */
    public function heeftEenVoornaam()
    {
        $voornaam = 'guido';

        $deelnemer = bewaarDeelnemer(['voornaam' => $voornaam]);

        $this->assertEquals(ucfirst($voornaam), $deelnemer->voornaam);
    }

    /** @test */
    public function heeftEenVoornaamBis()
    {
        $voornaam = 'GUIDO';

        $deelnemer = bewaarDeelnemer(['voornaam' => $voornaam]);

        $this->assertEquals(ucfirst($voornaam), $deelnemer->voornaam);
    }

    /** @test */
    public function heeftEenVolledigeNaam()
    {
        $naam = 'van hoof';
        $voornaam = 'guido';
        $volledigeNaam = strtoupper($naam) . ' ' . ucfirst($voornaam);

        $deelnemer = bewaarDeelnemer(['naam' => $naam, 'voornaam' => $voornaam]);

        $this->assertEquals($volledigeNaam, $deelnemer->volledigeNaam());
    }
}

