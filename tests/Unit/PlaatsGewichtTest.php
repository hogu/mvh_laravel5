<?php

namespace Tests\Unit;

use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class PlaatsGewichtTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function heeftEenPlaatsId()
    {
        $plaats_id = 1;

        $plaatsGewiht = bewaarPlaatsGewicht(['plaats_id' => $plaats_id]);

        $this->assertEquals($plaats_id, $plaatsGewiht->plaats_id);
    }

    /** @test */
    public function heeftEenGewicht()
    {
        $gewicht = 10;

        $plaatsGewicht = bewaarPlaatsGewicht(['gewicht' => $gewicht]);

        $this->assertEquals($gewicht, $plaatsGewicht->gewicht);
    }
//
//    /** @test */
//    public function plaatsDeelnemerIsUniek()
//    {
//        $this->expectException(QueryException::class);
//
//        $plaatsDeelnemer = bewaarPlaatsDeelnemer();
//        $plaatsDeelnemer = bewaarPlaatsDeelnemer();
//    }
}

