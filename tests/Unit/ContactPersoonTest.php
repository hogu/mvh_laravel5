<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mvh\ContactPersoon;
use Tests\TestCase;

class ContactPersoonTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function heeftEenNaam()
    {
        $naam = 'NAAM';

        $club = new ContactPersoon(['naam' => $naam]);

        $this->assertEquals($naam, $club->naam);
    }

    /** @test */
    public function heeftEenVoornaam()
    {
        $voornaam = 'VOORNAAM';

        $club = new ContactPersoon(['voornaam' => $voornaam]);

        $this->assertEquals($voornaam, $club->voornaam);
    }

    /** @test */
    public function heeftEenAdres()
    {
        $adres = 'STRAAT + NUMMER' . chr(10) . 'POSTCODE + GEMEENTE';

        $club = new ContactPersoon(['adres' => $adres]);

        $this->assertEquals($adres, $club->adres);
    }

    /** @test */
    public function heeftEenTelefoon()
    {
        $telefoon = '014 / 51 33 77';

        $club = new ContactPersoon(['telefoon' => $telefoon]);

        $this->assertEquals($telefoon, $club->telefoon);
    }

    /** @test */
    public function heeftEenEmailAdres()
    {
        $email = 'email@hotmail.com';

        $club = new ContactPersoon(['email' => $email]);

        $this->assertEquals($email, $club->email);
    }

    /** @test */
    public function heeftEenClub()
    {
        $club = bewaarClub();
        $contactPersoon = bewaarContactPersoon(['club_id' => $club->id]);

        $clubs = $contactPersoon->club();
        $actualClub = $clubs->first();

        $this->assertCount(1, $clubs);
        $this->assertEquals($club->id, $actualClub->id);
    }

    /** @test */
    public function heeftEenVolledigeNaam()
    {
        $naam = 'NAAM';
        $voornaam = 'VOORNAAM';
        $volledigeNaam = $naam . ' ' . $voornaam;

        $contactPersoon = bewaarContactPersoon(['naam' => $naam, 'voornaam' => $voornaam]);

        $this->assertEquals($volledigeNaam, $contactPersoon->volledigeNaam());
    }
}
