<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ArchiefKalendersRaadplegenTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function testArchiefKalendersKanGeraadpleegdWorden()
    {
        $kalenders = [
            bewaarKalender(),
            bewaarKalender()
        ];

        $response = $this->get('/archief/kalenders');

        $response->assertSee('Archief :');
        foreach ($kalenders as $kalender) {
            $response->assertSee($kalender->omschrijving());
        }
    }

    /** @test */
    public function testIndienGeenKalendersBeschikbaar()
    {
        $response = $this->get('/archief/kalenders');

        $response->assertSee('Geen kalenders beschikbaar !')
                ->assertSee('Archief :')
        ;
    }
}
