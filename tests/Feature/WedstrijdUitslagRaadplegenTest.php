<?php

namespace Tests\Feature;

use Mvh\Wedstrijd;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WedstrijdUitslagRaadplegenTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function testWedstrijdUitslagKanGeraadpleegdWorden()
    {
        $wedstrijd = maakWedstrijdUitslag();

        $response = $this->get('uitslag/' . $wedstrijd->id);

        $response->assertSee($wedstrijd->getDatum());
        $uitslagDetail = $wedstrijd->uitslagDetail();
        $response->assertSee(number_format($uitslagDetail['totaal'], 0, ',', '.'));
        foreach ($uitslagDetail['details'] as $detail) {
            $response
                ->assertSee(number_format($detail['volgnummer'], 0, ',', '.'))
                ->assertSee($detail['deelnemers'])
                ->assertSee(number_format($detail['totaal'], 0, ',', '.'))
            ;
            foreach ($detail['plaatsen'] as $plaats) {
                if ($plaats <> '') {
                    $response->assertSee(number_format($plaats, 0, ',', '.'));
                }
            }
            foreach ($detail['gewichten'] as $gewicht) {
                $response->assertSee(number_format($gewicht, 0, ',', '.'));
            }
        }
    }

    /** @test */
    public function testGeenWedstrijdUitslagBeschikbaar()
    {
        $kalender = bewaarKalender(['jaar' => date('Y')]);
        $wedstrijd = bewaarWedstrijd(['kalender_id' => $kalender->id]);

        $response = $this->get('uitslag/' . $wedstrijd->id);

        $response->assertSee('Geen uitslag beschikbaar !');
    }

}
