<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HomePageRaadplegenTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function testHomePageKanGeraadpleegdWorden()
    {
        $jaar = date('Y');
        $huidigeMaand = vandaag()->format('Y-m-d');
        $vorigeMaand = vandaag()->subDays(30)->format('Y-m-d');
        $volgendeMaand = vandaag()->addDays(30)->format('Y-m-d');
        $kalender = bewaarKalender(['jaar' => $jaar]);
        $vorigeWedstrijd = bewaarWedstrijd(
            ['kalender_id' => $kalender->id, 'datum' => $vorigeMaand]);
        $huidigeWedstrijd = bewaarWedstrijd(
            ['kalender_id' => $kalender->id, 'datum' => $huidigeMaand]);
        $volgendeWedstrijd = bewaarWedstrijd(
            ['kalender_id' => $kalender->id, 'datum' => $volgendeMaand]);

        $response = $this->get('/');

        $response->assertSee('Deze maand :');
        $response->assertDontSee($vorigeWedstrijd->getDatum());
        $response->assertSee($huidigeWedstrijd->getDatum());
        $response->assertDontSee($volgendeWedstrijd->getDatum());
    }

    /** @test */
    public function testIndienGeenWedstrijdDezeMaand()
    {
        $response = $this->get('/');

        $response->assertSee('Geen wedstrijden/evenementen deze maand !');
    }
}
