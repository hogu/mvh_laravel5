<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class KalenderRaadplegenTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function testKalenderKanGeraadpleegdWorden()
    {
        $kalender = maakKalenderOp(2);

        $response = $this->get('kalender/' . $kalender->id);

        $response->assertSee($kalender->omschrijving());

        foreach ($kalender->wedstrijden()->get() as $wedstrijd) {
            $response->assertSee($wedstrijd->omschrijving);
            $response->assertSee($wedstrijd->getDatum());
            $response->assertSee($wedstrijd->getAanvang());
        }
    }

    /** @test */
    public function testGeenWedstrijdenBeshikbaar()
    {
        $kalender = bewaarKalender(['jaar' => date('Y')]);

        $response = $this->get('kalender/' . $kalender->id);

        $response
            ->assertSee($kalender->omschrijving())
            ->assertSee('Geen wedstrijden beschikbaar !')
        ;
    }
}
