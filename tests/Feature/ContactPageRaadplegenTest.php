<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ContactPageRaadplegenTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function testContactPageKanGeraadpleegdWorden()
    {
        $club = bewaarClub();

        $response = $this->get('contact');

        $response->assertSee($club->naam);
        $response->assertSee($club->adres);
        $response->assertSee($club->telefoon);
        $response->assertSee($club->email);
        $response->assertSee('Geen contactpersonen beschikbaar !');
    }

    /** @test */
    public function testGeenContactGegevensBeschikbaar()
    {
        $response = $this->get('contact');

        $response->assertSee('Geen contactgegevens beschikbaar !');
    }

    /** @test */
    public function testContactpersonenZichtbaar()
    {
        $club = bewaarClub();
        $eersteContactPersoon = bewaarContactPersoon(['club_id' => $club->id]);
        $tweedeContactPersoon = bewaarContactPersoon(['club_id' => $club->id]);

        $response = $this->get('contact');

        $response->assertSee($club->naam);
        $response->assertSee($club->adres);
        $response->assertSee($club->telefoon);
        $response->assertSee($club->email);

        $contactpersonen = $club->contactpersonen()->get();
        foreach ($contactpersonen as $contactpersoon) {
            $response->assertSee($contactpersoon->volledigeNaam());
            $response->assertSee($contactpersoon->telefoon);
            $response->assertSee($contactpersoon->email);
        }
    }
}
