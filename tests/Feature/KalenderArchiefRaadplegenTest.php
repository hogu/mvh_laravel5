<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class KalenderArchiefRaadplegenTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function testKalenderArchiefKanGeraadpleegdWorden()
    {
        $kalender = maakKalenderOp();

        $response = $this->get('/archief/kalender/' . $kalender->id);

        $response
                ->assertSee('Kalender Archief :')
                ->assertSee($kalender->omschrijving())
        ;
        $wedstrijden = $kalender->wedstrijden()->get();
        foreach ($wedstrijden as $wedstrijd) {
            $response->assertSee($wedstrijd->getDatum());
        }
    }

    /** @test */
    public function testIndienGeenWedstrijdenBeschikbaar()
    {
        $kalender = bewaarKalender(['jaar' => date('Y')]);

        $response = $this->get('/archief/kalender/' . $kalender->id);

        $response
                ->assertSee('Geen wedstrijden beschikbaar !')
                ->assertSee('Kalender Archief :')
        ;
    }
}
