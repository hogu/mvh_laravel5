<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UitslagenRaadplegenTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function testUitslagenKanGeraadpleegdWorden()
    {
        $kalender = maakKalenderOp(2);

        $response = $this->get('uitslagen');

        $response->assertSee('Uitslagen ' . $kalender->omschrijving());

        foreach ($kalender->wedstrijden()->get() as $wedstrijd) {
            $response->assertSee($wedstrijd->omschrijving);
            $response->assertSee($wedstrijd->getDatum());
            $response->assertDontSee($wedstrijd->getAanvang());
        }
    }

    /** @test */
    public function testGeenUitslagenBeschikbaar()
    {
        $response = $this->get('uitslagen');

        $response->assertSee('Geen uitslagen beschikbaar !');
    }}
