<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class KalenderPageRaadplegenTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function testKalenderPageKanGeraadpleegdWorden()
    {
        $uri = '/kalender';
        $kalender = maakKalenderOp();

        $response = $this->get($uri);

        $response->assertRedirect($uri . '/' . $kalender->id);
    }

    /** @test */
    public function testIndienGeenKalenderAanwezig()
    {
        $response = $this->get('/kalender');

        $response->assertSee('Geen kalender beschikbaar !');
    }
}
