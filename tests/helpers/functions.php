<?php

use Carbon\Carbon;
use Mvh\Club;
use Mvh\ContactPersoon;
use Mvh\Deelnemer;
use Mvh\Kalender;
use Mvh\Plaats;
use Mvh\PlaatsDeelnemer;
use Mvh\PlaatsGewicht;
use Mvh\Reeks;
use Mvh\Wedstrijd;
use Mvh\WedstrijdDeelnemer;
use Mvh\WedstrijdType;

function bewaarReeks($velden = [])
{
    return factory(Reeks::class)->create($velden);
}

function maakReeks($velden = [])
{
    return factory(Reeks::class)->make($velden);
}

function bewaarWedstrijd($velden = [])
{
    return factory(Wedstrijd::class)->create($velden);
}

function maakWedstrijd($velden = [])
{
    return factory(Wedstrijd::class)->make($velden);
}

function bewaarWedstrijdType($velden = [])
{
    return factory(WedstrijdType::class)->create($velden);
}

function maakWedstrijdType($velden = [])
{
    return factory(WedstrijdType::class)->make($velden);
}

function maakPlaats($velden = [])
{
    return factory(Plaats::class)->make($velden);
}

function bewaarPlaats($velden = [])
{
    return factory(Plaats::class)->create($velden);
}

function maakWedstrijdDeelnemer($velden = [])
{
    return factory(WedstrijdDeelnemer::class)->make($velden);
}

function bewaarWedstrijdDeelnemer($velden = [])
{
    return factory(WedstrijdDeelnemer::class)->create($velden);
}

function maakDeelnemer($velden = [])
{
    return factory(Deelnemer::class)->make($velden);
}

function bewaarDeelnemer($velden = [])
{
    return factory(Deelnemer::class)->create($velden);
}

function maakPlaatsDeelnemer($velden = [])
{
    return factory(PlaatsDeelnemer::class)->make($velden);
}

function bewaarPlaatsDeelnemer($velden = [])
{
    return factory(PlaatsDeelnemer::class)->create($velden);
}

function maakPlaatsGewicht($velden = [])
{
    return factory(PlaatsGewicht::class)->make($velden);
}

function bewaarPlaatsGewicht($velden = [])
{
    return factory(PlaatsGewicht::class)->create($velden);
}

function maakWedstrijdUitslag($aantalDeelnemers = 10, $aantalReeksen = 3)
{
    // wedstrijdtype
    $wedstrijdType = bewaarWedstrijdType();
    // wedstrijd
    $wedstrijd = bewaarWedstrijd(['wedstrijdtype_id' => $wedstrijdType->id]);
    // deelnemers
    $deelnemers = bewaarDeelnemers($aantalDeelnemers);
    // wedstrijd - deelnemers
    $wedstrijdDeelnemers = bewaarWedstrijdDeelnemers($deelnemers, $wedstrijd);
    // reeksen
    $reeksen = bewaarReeksen($aantalReeksen, $wedstrijd);
    // plaatsen
    foreach ($reeksen as $reeks) {
        $plaatsen = bewaarPlaatsen($aantalDeelnemers, $reeks);
        // plaats - deelnemers
        bewaarPlaatsDeelnemers($plaatsen, $wedstrijdDeelnemers);
        // plaats - gewichten
        bewaarPlaatsGewichten($plaatsen);
    }
    return $wedstrijd;
}

/**
 * @param $plaatsen
 */
function bewaarPlaatsGewichten($plaatsen)
{
    $aantal = 1;
    foreach ($plaatsen as $plaats) {
        bewaarPlaatsGewicht(
            ['plaats_id' => $plaats->id, 'gewicht' => $aantal * 100]
        );
        $aantal++;
    }
}

/**
 * @param $plaatsen
 * @param $wedstrijdDeelnemers
 * @return array
 */
function bewaarPlaatsDeelnemersMarathon($plaatsen, $wedstrijdDeelnemers, $volgnummer)
{
    // to do uitvogelen hoe reeks 1 <> reeks 2 en reeks 3
    $aantal = $volgnummer === 1 ? 0 : count($wedstrijdDeelnemers) -1;
    foreach ($plaatsen as $plaats) {
        bewaarPlaatsDeelnemer(
            ['plaats_id' => $plaats->id, 'wedstrijd_deelnemer_id' => $wedstrijdDeelnemers[$aantal]->id]
        );
        $volgnummer === 1 ? $aantal++ : $aantal--;
    }
}

/**
 * @param $plaatsen
 * @param $wedstrijdDeelnemers
 * @return array
 */
function bewaarPlaatsDeelnemersKoppel($plaatsen, $wedstrijdDeelnemers)
{
    $aantal = 0;
    foreach ($plaatsen as $plaats) {
        bewaarPlaatsDeelnemer(
            ['plaats_id' => $plaats->id, 'wedstrijd_deelnemer_id' => $wedstrijdDeelnemers[$aantal]->id]
        );
        $aantal++;
        bewaarPlaatsDeelnemer(
            ['plaats_id' => $plaats->id, 'wedstrijd_deelnemer_id' => $wedstrijdDeelnemers[$aantal]->id]
        );
        $aantal++;
    }
}

/**
 * @param $plaatsen
 * @param $wedstrijdDeelnemers
 * @return array
 */
function bewaarPlaatsDeelnemers($plaatsen, $wedstrijdDeelnemers)
{
    $aantal = 0;
    foreach ($plaatsen as $plaats) {
        bewaarPlaatsDeelnemer(
            ['plaats_id' => $plaats->id, 'wedstrijd_deelnemer_id' => $wedstrijdDeelnemers[$aantal]->id]
        );
        $aantal++;
    }
}

/**
 * @param $aantalDeelnemers
 * @param $reeks
 * @return array
 */
function bewaarPlaatsenKoppel($aantalDeelnemers, $reeks)
{
    $plaatsen = [];
    for ($i = 1; $i <= $aantalDeelnemers / 2; $i++) {
        $plaatsen[] = bewaarPlaats(['reeks_id' => $reeks->id, 'nummer' => $i]);
    }
    return $plaatsen;
}
/**
 * @param $aantalDeelnemers
 * @param $reeks
 * @return array
 */
function bewaarPlaatsen($aantalDeelnemers, $reeks)
{
    $plaatsen = [];
    for ($i = 1; $i <= $aantalDeelnemers; $i++) {
        $plaatsen[] = bewaarPlaats(['reeks_id' => $reeks->id, 'nummer' => $i]);
    }
    return $plaatsen;
}

/**
 * @param $aantalReeksen
 * @param $wedstrijd
 * @return array
 */
function bewaarReeksen($aantalReeksen, $wedstrijd)
{
    $reeksen = [];
    for ($i = 1; $i <= $aantalReeksen; $i++) {
        $reeksen[] = bewaarReeks(['wedstrijd_id' => $wedstrijd->id, 'volgnummer' => $i]);
    }
    return $reeksen;
}

/**
 * @param $deelnemers
 * @param $wedstrijd
 * @return array
 */
function bewaarWedstrijdDeelnemers($deelnemers, $wedstrijd)
{
    $wedstrijdDeelnemers = [];
    foreach ($deelnemers as $deelnemer) {
        $wedstrijdDeelnemers[] = bewaarWedstrijdDeelnemer(
            ['wedstrijd_id' => $wedstrijd->id, 'deelnemer_id' => $deelnemer->id]
        );
    }
    return $wedstrijdDeelnemers;
}

/**
 * @param $aantalDeelnemers
 * @return array
 */
function bewaarDeelnemers($aantalDeelnemers)
{
    $deelnemers = [];
    for ($i = 1; $i <= $aantalDeelnemers; $i++) {
        $deelnemers[] = bewaarDeelnemer(
            ['nummer' => $i, 'naam' => 'Naam' . $i, 'voornaam' => 'Voornaam' . $i]
        );
    }
    return $deelnemers;
}

function maakKoppelUitslag($aantalDeelnemers = 10, $aantalReeksen = 3)
{
    // wedstrijdtype
    $wedstrijdType = bewaarWedstrijdType();
    // wedstrijd
    $wedstrijd = bewaarWedstrijd(['wedstrijdtype_id' => $wedstrijdType->id]);
    // deelnemers
    $deelnemers = bewaarDeelnemers($aantalDeelnemers);
    // wedstrijd - deelnemers
    $wedstrijdDeelnemers = bewaarWedstrijdDeelnemers($deelnemers, $wedstrijd);
    // reeksen
    $reeksen = bewaarReeksen($aantalReeksen, $wedstrijd);
    // plaatsen
    foreach ($reeksen as $reeks) {
        $plaatsen = bewaarPlaatsenKoppel($aantalDeelnemers, $reeks);
        // plaats - deelnemers
        bewaarPlaatsDeelnemersKoppel($plaatsen, $wedstrijdDeelnemers);
        // plaats - gewichten
        bewaarPlaatsGewichten($plaatsen);
    }
    return $wedstrijd;
}

function maakMarathonUitslag($aantalDeelnemers = 10, $aantalReeksen = 3)
{
    // wedstrijdtype
    $wedstrijdType = bewaarWedstrijdType();
    // wedstrijd
    $wedstrijd = bewaarWedstrijd(['wedstrijdtype_id' => $wedstrijdType->id]);
    // deelnemers
    $deelnemers = bewaarDeelnemers($aantalDeelnemers);
    // wedstrijd - deelnemers
    $wedstrijdDeelnemers = bewaarWedstrijdDeelnemers($deelnemers, $wedstrijd);
    // reeksen
    $reeksen = bewaarReeksen($aantalReeksen, $wedstrijd);
    // plaatsen
    foreach ($reeksen as $reeks) {
        $plaatsen = bewaarPlaatsen($aantalDeelnemers, $reeks);
        // plaats - deelnemers
        bewaarPlaatsDeelnemersMarathon($plaatsen, $wedstrijdDeelnemers, $reeks->volgnummer);
        // plaats - gewichten
        bewaarPlaatsGewichten($plaatsen);
    }
    return $wedstrijd;
}

function maakKalenderOp($aantalWedstrijden = 10)
{
    $kalender = bewaarKalender();

    for ($i = 1; $i <= $aantalWedstrijden; $i++) {
        $wedstrijd = maakWedstrijd(['omschrijving' => 'Wedstrijd ' . $i, 'opmerkingen' => null]);
        $kalender->wedstrijden()->save($wedstrijd);
    }

    return $kalender;
}

function bewaarKalender($velden = [])
{
    return factory(Kalender::class)->create($velden);
}
function maakKalender($velden = [])
{
    return factory(Kalender::class)->make($velden);
}

function bewaarClub($velden = [])
{
    return factory(Club::class)->create($velden);
}

function maakClub($velden = [])
{
    return factory(Club::class)->make($velden);
}

function bewaarContactPersoon($velden = [])
{
    return factory(ContactPersoon::class)->create($velden);
}

function maakContactPersoon($velden = [])
{
    return factory(ContactPersoon::class)->make($velden);
}

function vandaag()
{
    return Carbon::now();
}